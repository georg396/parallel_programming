#include "tbb/task_scheduler_init.h"
#include "tbb/parallel_for.h"
#include "tbb/blocked_range.h"
#include "tbb/spin_mutex.h"
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <ctime>

using namespace std;
using namespace tbb;

void work(string str, vector<string> other, int& pos, int& length) {
	for (int i = 0; i < str.length(); i++) {
		for (int j = 1; j < str.length() - i + 1; j++) {
			string check = str.substr(i, j);
			bool found = true;
			for (int i = 0; i < other.size(); i++) {
				if (other[i].find(check) == -1)
					found = false;
			}

			if (found == true)
				if (check.length() > length)
				{
					pos = i;
					length = j;
				}
		}
	}
}

class SubstringFinder {
private:
	static int pos;
	static int length;
	string str;
	vector<string> other;
	static spin_mutex myMutex;
public:
	SubstringFinder(int p, int l, string s, vector<string> o) {
		pos = p;
		length = l;
		str = s;
		other = o;
	}

	int getPos() { return pos; }
	int getLen() { return length; }

	void operator()(const blocked_range<int>& range) const {			int begin = range.begin(), end = range.end();		for (int i = begin; i < end; i++) {
			for (int j = 1; j < str.length() - i + 1; j++) {
				string check = str.substr(i, j);
				bool found = true;
				for (int i = 0; i < other.size(); i++) {
					if (other[i].find(check) == -1)
						found = false;
				}

				if (found == true)
					if (check.length() > length)
					{
						spin_mutex::scoped_lock lock(myMutex);
						pos = i;
						length = j;
					}
			}
		}	}
};

int factorial(int n)
{
	return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

int SubstringFinder::pos;
int SubstringFinder::length;
spin_mutex SubstringFinder::myMutex;

int main(int argc, char** argv[]) {
	int thread_num = 4;
	task_scheduler_init init(thread_num);
	
	//init.initialize();
	//time_t start_time, stop_time;
	clock_t stop_time;

	string str = "aatyuioddd";

	/*int count = 0;
	for (int i = 1; i <= str.length(); i++) {
	count += factorial(i);
	}
	cout << count << endl;*/

	vector<string> strings;
	for (int i = 0; i < 10000; i++) {
		strings.push_back("aaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddd");
		strings.push_back("aaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddd");
	}

	clock_t begin_time = clock();
	SubstringFinder sf(0, 0, str, strings);
	parallel_for(blocked_range<int>(0, str.length()), sf);
	stop_time = clock();
	init.terminate();

	float parr_time = float(stop_time - begin_time) / CLOCKS_PER_SEC;
	cout << "Parallel time on " << thread_num << " threads: " << parr_time << endl;
	cout << str.substr(sf.getPos(), sf.getLen()) << endl;

	/*init.initialize(1);
	begin_time = clock();
	SubstringFinder sf2(0, 0, str, strings);
	parallel_for(blocked_range<int>(0, str.length()), sf2);
	stop_time = clock();
	init.terminate();*/

	int _pos = 0, _len = 0;
	begin_time = clock();
	work(str, strings, _pos, _len);
	stop_time = clock();

	float single_time = float(stop_time - begin_time) / CLOCKS_PER_SEC;
	cout << "Single time: " << single_time << endl;
	//cout << str.substr(sf2.getPos(), sf2.getLen()) << endl;
	cout << str.substr(_pos, _len) << endl;

	cout << "Speedup: " << single_time / parr_time << endl;

	return 0;
}