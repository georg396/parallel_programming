#include "mpi.h"
#include <omp.h>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <ctime>

using namespace std;

void work(int proc_num, int len, string str, vector<string> other, int*& pos_len) {
	int a;
	MPI_Comm_rank(MPI_COMM_WORLD, &a);
	int start = len * a;
	int len_;
	if (a == proc_num - 1)
		len_ = str.length() - start + 1;
	else len_ = len + 1;
	//cout << "Proc: " << a << ", start: " << start << ", len: " << len_ << endl;

	#pragma omp parallel for 
	for (int i = start; i < start + len_ - 1; i++) {
		for (int j = 1; j < str.length() - i + 1; j++) {
			string check = str.substr(i, j);
			//cout << check << endl;
			bool found = true;
			for (int i = 0; i < other.size(); i++) {
				if (other[i].find(check) == -1)
					found = false;
			}

			if (found == true)
				if (check.length() > pos_len[a*2+1])
				#pragma omp critical
				{
					pos_len[a * 2] = i;
					pos_len[a * 2 + 1] = j;
				}
		}
	}
}

int factorial(int n)
{
	return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

int main(int argc, char* argv[]) {
	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	//cout << "I am proc with rank " << rank << endl;

	int proc_num;
	MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
	int array_size = proc_num * 2;
	int thread_num = 2;
	omp_set_num_threads(thread_num);

	string str = "aqwerqweratyuibbbbboqwerqwerdddd";

	vector<string> strings;
	for (int i = 0; i < 200000; i++) {
		strings.push_back("aaaaaaaaaaaaaaaaaaaabbbbbbbbaaaddddddddddddddddd");
	}

	int* send = new int[array_size];
	int* pos_len = new int[array_size];
	for (int i = 0; i < array_size; i++) {
		send[i] = 0;
		pos_len[i] = 0;
	}

	clock_t begin_time = clock();

	work(proc_num, str.length() / proc_num, str, strings, send);

	MPI_Reduce(send, pos_len, array_size, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	if (rank == 0) {
		int max_pos = 0, max_len = 0;
		for (int i = 0; i < array_size; i += 2) {
			if (pos_len[i + 1] > max_len) {
				max_len = pos_len[i + 1];
				max_pos = pos_len[i];
			}
		}
		clock_t stop_time = clock();

		float parr_time = float(stop_time - begin_time) / CLOCKS_PER_SEC;
		cout << "Parallel time on " << proc_num << " processes: " << parr_time << endl;
		cout << str.substr(max_pos, max_len) << endl;

		proc_num = 1;
		for (int i = 0; i < array_size; i++) {
			pos_len[i] = 0;
		}
		begin_time = clock();
		work(proc_num, str.length() / proc_num, str, strings, pos_len);
		stop_time = clock();

		float single_time = float(stop_time - begin_time) / CLOCKS_PER_SEC;
		cout << "Single time: " << single_time << endl;
		cout << str.substr(pos_len[0], pos_len[1]) << endl;

		cout << "Speedup :" << single_time / parr_time << endl;
	}

	MPI_Finalize();

	return 0;
}