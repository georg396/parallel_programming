#include <omp.h>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <ctime>

using namespace std;

void work(int thread_num, int len, string str, vector<string> other, int& pos, int& length) {
	int a = omp_get_thread_num();
	int start = len * a;
	int len_;
	if (a == thread_num - 1)
		len_ = str.length() - start + 1;
	else len_ = len + 1;

	//cout << a << endl;
	for (int i = start; i < start + len_ - 1; i++) {
		for (int j = 1; j < str.length() - i + 1; j++) {
			string check = str.substr(i, j);
			/*if (a == 3)
				cout << a << ": " << str.substr(i, j) << endl;*/
			bool found = true;
			for (int i = 0; i < other.size(); i++) {
				if (other[i].find(check) == -1)
					found = false;
			}

			if (found == true)
				if (check.length() > length) 
				#pragma omp critical
				{
					pos = i;
					length = j;
					//cout << a << ": " << str.substr(i, j) << endl;
				}
		}
	}
}

int factorial(int n)
{
	return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

int main(int argc, char** argv[]) {

	int thread_num = 2;
	omp_set_num_threads(thread_num);
	//time_t start_time, stop_time;
	clock_t stop_time;

	string str = "aatyuioddd";

	/*int count = 0;
	for (int i = 1; i <= str.length(); i++) {
		count += factorial(i);
	}
	cout << count << endl;*/

	vector<string> strings;
	for (int i = 0; i < 10000; i++) {
		strings.push_back("aaaaaaaaaaaaaaaaaaaaaaaddddddddddddddddd");
		strings.push_back("aaaaaaaaaaaaaaaaaaaaaaaaaddddddddddddd");
	}

	//start_time = time(NULL);
	//cout << strings.size() << endl;


	clock_t begin_time = clock();
	int pos = 0, length = 0;
	#pragma omp parallel
		work(thread_num, str.length() / thread_num, str, strings, pos, length);
	stop_time = clock();
	//stop_time = time(NULL);

	cout << "Parallel time on " << thread_num << " threads: " << float(stop_time - begin_time)/CLOCKS_PER_SEC << endl;
	cout << str.substr(pos, length) << endl;

	//cout << "Max substring: " << max_substring << endl;

	thread_num = 1;
	begin_time = clock();
	pos = 0; length = 0;
	work(thread_num, str.length() / thread_num, str, strings, pos, length);
	stop_time = clock();

	cout << "Single time: " << float(stop_time - begin_time) / CLOCKS_PER_SEC << endl;
	cout << str.substr(pos, length) << endl;

	return 0;
}